+++
title = "Hallo Welt"
date = "2022-12-15T22:00:53+01:00"
author = "Rinma"
authorTwitter = "" #do not include @
cover = "https://og-image.vercel.app/Hello%20World.png"
tags = ["Sonstiges"]
keywords = ["Sonstiges"]
description = "Mein erster Blogbeitrag mit Informationen zu mir und dem Ziel dieses Blogs"
showFullContent = false
readingTime = true
hideComments = false
color = "" #color from the theme settings
+++

Hello World,
These are words that you often read when you start learning a programming language. One of the most used examples, and a simple one.

{{< code language="javascript" title="Hello World" expand="Anzeigen" collapse="Verstecken" isCollapsed="false" >}}
let name = "Rinma";
console.log(`Hello ${name}`);
{{< /code >}}

Hello world, my name is Rinma. I've been a software developer for over 11 years and have a great deal of interest 
in many topics, including software development, game development,
and hardware development.
I also have hobbies away from my computer, but they don't matter here :)

Most of the time I try to use open-source software to achieve my goals, most of the time it works.
For example: I make games using Godot engine, Blender for 3D modelling, LMMS for music, Gimp/Krita and Inkscape for graphics,
or get free resources from [AmbientCG](https://ambientcg.com/), [OpenGameArt](https://opengameart.org/), [Kenney](https://kenney.nl/)
and [Itch](https://itch.io/game-assets).

This blog is mostly for me, writing down stuff I learned and want to memorize, a place where I can go and search for the knowledge that
I already forgot again. This means, I have to write it down that I can fast and easily gain this knowledge back, and maybe it helps you too.

Till the next post,  
Rinma